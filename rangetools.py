#! /usr/bin/python3

""" rangetools.py

Part of Alan's Python Tiny Tools https://gitlab.com/AlanDeSmet/python-tinytools

MIT License

Copyright (c) 2021 Alan De Smet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# SPDX-License-Identifier: MIT


def anyrange(start, stop=None, step=1, inclusive=False, zero_step=None):
    """ Return object producing range of items from start stop in step increments

    Should be able to act as a drop-in replacement for range, but with support
    for anything that behaves reasonably like a number, including floats,
    datetime.datetime, datetime.date, fractions.Fraction, and decimal.Decimal.

    if stop is None, start will be used as stop and start will be "zero"
    (type(start)()).

    Similar to range, will NOT include stop as a result by default. Use
    inclusive=True to include stop, assuming it's an even multiple of step.

    type(step)() should construct a "zero" version of step which has no result
    if added to start. If this does not work, set zero_step explicitly.

    >>> list(anyrange(3))
    [0, 1, 2]
    >>> list(anyrange(3, inclusive=True))
    [0, 1, 2, 3]
    >>> list(anyrange(5,-3,-2))
    [5, 3, 1, -1]
    >>> list(anyrange(1.1,1.5,.2))
    [1.1, 1.3]
    >>> from datetime import datetime, timedelta
    >>> list(anyrange(datetime(2000, 1, 1), datetime(1999,12,30),
    ...     timedelta(days=-1)))
    [datetime.datetime(2000, 1, 1, 0, 0), datetime.datetime(1999, 12, 31, 0, 0)]
    >>> from fractions import Fraction
    >>> list(anyrange(Fraction(1,3), Fraction(2,3), Fraction(1,6)))
    [Fraction(1, 3), Fraction(1, 2)]
    >>> list(anyrange(Fraction(1,3), Fraction(5,3)))
    [Fraction(1, 3), Fraction(4, 3)]
    >>> from decimal import Decimal
    >>> list(anyrange(Decimal("1.33"), Decimal("1.66"), Decimal("0.11")))
    [Decimal('1.33'), Decimal('1.44'), Decimal('1.55')]
    """
    import operator

    if zero_step is None:
        try:
            zero_step = type(step)()
        except Exception as e:
            raise TypeError(f"Unable to contruct zero of the step type {type(step)}; you may need to explicitly set the zero_step value. (Error encountered: {str(e)}")

    try:
        _ = step == zero_step
        _ = step < zero_step
    except Exception as e:
        raise ValueError(f"Unable to compare step ({type(step)}) to zero_step ({type(zero_step)}) for equality; you may need to explicitly set the zero_step value. (Error encountered: {str(e)}")

    if step == zero_step:
        raise ValueError("anyrange() arg 3 (step) must not be zero")

    if stop is None:
        stop = start
        try:
            start = type(stop)()
        except Exception as e:
            raise TypeError(f"Unable to construct zero of type {type(stop)}; you may need to explicitly set the start value. (Error encountered: {str(e)})")

    try: # Ensure start and step are compatible
        tmp = start + step
    except Exception as e:
        msg = f"Unable to add start ({type(start)}) to step ({type(step)}). The two types must be compatible."
        if step == 1:
            msg += " You may need to explicitly set step."
        raise TypeError(msg)

    try: # Ensure (start+step) and stop are compatible
        _ = tmp < stop
        _ = tmp > stop
        _ = tmp == stop
    except Exception as e:
        raise TypeError(f"Unable to compare start+step ({type(tmp)} {tmp}) to stop ({type(stop)} {stop}). The types must be compatible.")

    compare = operator.lt
    if step < zero_step:
        compare = operator.gt
    current = start
    while compare(current, stop):
        yield current
        current += step
    if inclusive and current == stop:
        yield current


import unittest

class _anyrangeTester(unittest.TestCase):

    def assertList(self, alist, blist, inner_type=None):
        alist = list(alist)
        blist = list(blist)
        err = AssertionError(f"{alist} != {blist}")
        if len(alist) != len(blist): raise err
        for a,b in zip(alist,blist):
            if inner_type is not None:
                b = inner_type(b)
            if a != b or type(a) != type(b): raise err


    def basic_tests(self, result_type):
        def _assert(a,b):
            self.assertList(a,b,result_type)
        t=result_type
        _assert( anyrange(t(3)                           ), [0,1,2]   )
        _assert( anyrange(t(3),            inclusive=True), [0,1,2,3] )
        _assert( anyrange(t(3),t( 5)                     ), [3,4]     )
        _assert( anyrange(t(3),t( 5),      inclusive=True), [3,4,5]   )
        _assert( anyrange(t(5),t( 3)                     ), []        )
        _assert( anyrange(t(5),t( 3),t(-1)               ), [5,4]     )
        _assert( anyrange(t(9),t( 3),t(-2)               ), [9,7,5]   )
        _assert( anyrange(t(3),t(10),t(2)                ), [3,5,7,9] )
        _assert( anyrange(t(3),t(10),t(2), inclusive=True), [3,5,7,9] )
        _assert( anyrange(t(-3)                          ), []        )
        _assert( anyrange(t(-3),t(-1)                    ), [-3,-2]   )
        _assert( anyrange(t(-1),t(-5),t(-2)              ), [-1,-3]   )
        _assert( anyrange(t(2),t(-2),t(-3)               ), [2,-1]    )

    def test_int(self):
        self.basic_tests(int)

    def test_float(self):
        self.basic_tests(float)
        # These tests are suspicious and should probably
        # explicitly allow for floating point inaccuracy.
        self.assertList( anyrange(.1, 1.1), [.1])
        self.assertList( anyrange(.1, 1.1, inclusive=True), [.1,1.1])
        self.assertList( anyrange(.1, 1.3, .3), [.1,.4,.7,1.0])
        self.assertList( anyrange(.2, -.4, -.2), [0.2,0.0,-0.2])

    def test_datetime(self):
        from datetime import datetime as dt, timedelta as delt, date
        self.assertList(
                anyrange(dt(2019,12,30), dt(2020,1,1), delt(days=1)),
                [dt(2019,12,30),dt(2019,12,31)])
        self.assertList(
                anyrange(dt(2020,1,1), dt(2019,12,30), delt(days=-1)),
                [ dt(2020,1,1), dt(2019,12,31)])
        self.assertList(
                anyrange(dt(2020,1,1), dt(2019,12,30), delt(days=1)),
                [])
        self.assertList(
                anyrange(dt(2019,12,30), dt(2020,1,1), delt(days=1), inclusive=True),
                [dt(2019,12,30),dt(2019,12,31),dt(2020,1,1)])
        self.assertList(
                anyrange(date(2019,12,30), date(2020,1,1), delt(days=1)),
                [date(2019,12,30),date(2019,12,31)])
        self.assertList(
                anyrange(date(2019,12,30), date(2020,1,1), delt(days=1), inclusive=True),
                [date(2019,12,30),date(2019,12,31),date(2020,1,1)])
        self.assertList(
                anyrange(dt(2020,6,6,3,30,10), dt(2020,6,6,3,31,40), delt(seconds=30)),
                [dt(2020, 6, 6, 3, 30, 10), dt(2020, 6, 6, 3, 30, 40), dt(2020, 6, 6, 3, 31, 10)])

    def test_fractions(self):
        from fractions import Fraction as F
        self.assertList( anyrange(F(3,1)), [F(0),F(1),F(2)])
        self.assertList( anyrange(F(1,3), F(3,3), F(1,3)), [F(1,3),F(2,3)])
        self.assertList( anyrange(F(7,3), F(4,3), F(-1,3)), [F(7,3),F(6,3),F(5,3)])
        self.assertList( anyrange(F(1,3), F(-4,9), F(-1,3)), [F(1,3),F(0),F(-1,3)])

    def test_decimal(self):
        from decimal import Decimal as D
        self.assertList( anyrange(D(3)), [D(0),D(1),D(2)])
        self.assertList( anyrange(D('3'), D('3.1492'), D('0.05')), [D('3.00'),D('3.05'),D('3.10')])
        self.assertList( anyrange(D('1.2'), D('-1.1'), D('-0.8')), [D('1.2'),D('0.4'),D('-0.4')])

    def test_str(self):
        # This is deeply silly, but it should work
        self.assertList(anyrange("x","xAAA","A"),["x", "xA", "xAA"])

    def test_errors(self):
        from datetime import datetime, timedelta
        with self.assertRaisesRegex(TypeError, 'Unable to add start.*to step'):
            list(anyrange(""))
        with self.assertRaisesRegex(TypeError, 'Unable to add start.*to step'):
            list(anyrange(datetime(2020,1,1),datetime(2020,1,2)))
        with self.assertRaisesRegex(TypeError, 'Unable to construct zero of type'):
            list(anyrange(datetime(2020,1,1)))
        with self.assertRaisesRegex(ValueError, 'Unable to compare step.*to zero_step'):
            list(anyrange(1,2,3,zero_step=""))
        with self.assertRaisesRegex(ValueError, r'anyrange\(\) arg 3 \(step\) must not be zero'):
            list(anyrange(datetime(2020,1,1),datetime(2020,1,2), timedelta()))

def load_tests(loader, tests, ignore):
    """ Ensure unittest runs doctest

    You can run the tests with:

    python3 rangetools.py
    """
    import doctest
    tests.addTests(doctest.DocTestSuite())
    return tests


if __name__ == '__main__':
    unittest.main()
