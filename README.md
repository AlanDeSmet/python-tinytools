Alan's Python Tiny Tools
========================

A random collection of small libraries for Python, typically requiring at lest
Python 3.6.

- **rangetools.anyrange** - like the built-in function range, but works on a wide variety of number-like things, including float, datetime.datetime, fractions.Fraction, and decimal.Decimal.
